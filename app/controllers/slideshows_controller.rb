class SlideshowsController < ApplicationController
  before_action :set_user, only: [:new, :index, :create, :show, :edit, :destroy] 

  def index
    @slideshows = @user.slideshows.all
  end

  def new
  end

  def create
    @slideshow = @user.slideshows.new
    @slideshow[:imgs] = params[:slides]
    if @slideshow.save
      @user.slideshows.push(@slideshow)
      render json: { message: "success"}, :status => 200
      flash[:notice] = "Slideshow successfully created"
      redirect_to root_path
    else
      render json: { error: @slideshow.errors.full_messages.join(',') }, status => 400
    end
  end

  def show
    slideshow = Slideshow.find(params[:id])
    if !slideshow
      format.html {redirect_to slideshows_url, notice: "Slideshow not found"}
      return
    end
    uploads = Upload.all
    @images_publ_ids = slideshow[:imgs].split(";")
  end

  def save
    
  end

  def destroy
    @user.slideshows.find(params[:id]).destroy
    respond_to do |format|
      format.html { redirect_to user_slideshows_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end

  end

 private
    def set_user
      @user = User.find(params[:user_id])
    end
  
end
