class CreateSlideshows < ActiveRecord::Migration[5.0]
  def change
    create_table :slideshows do |t|
      t.text :imgs
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
