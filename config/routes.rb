Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_scope :user do
    root :to => 'devise/sessions#new'
    get "/users/sign_out" => 'devise/sessions#destroy'
  end

  resources :users do
    resources :uploads
    resources :slideshows
  end

  match 'users/:user_id/uploads/update' => 'uploads#update', :via => :post
  match 'users/:user_id/slideshows/create' => 'slideshows#create', :via => :post
end
